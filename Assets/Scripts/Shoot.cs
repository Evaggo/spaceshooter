﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float speed;
    public Vector2 diretion;

    private void Update()
    {
        transform.Translate (diretion * speed * Time.deltaTime); //Vector2.right = constante que tira siempre pa la derecha
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Meteor")
        {
            Destroy(gameObject);
        }
    }

}
