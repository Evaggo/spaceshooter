﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 speed;
    int select;

    public GameObject [] meteors;
    public AudioSource explosionSound;
    public ParticleSystem explosion;
    public GameObject meteorToInstanciate;

    private void Awake()
    {
        //Para ocultar los meteoritos
        for (int i = 0; i < meteors.Length; i++)
        {
            meteors[i].SetActive(false);
        }

        //Para seleccionar de manera aleatoria cual se activa
        select = Random.Range(0, meteors.Length);
        meteors[select].SetActive(true);

        //Velocidad random
        speed.x = Random.Range(-5,0);
        speed.y = Random.Range(-3, 3);
    }

    private void Update()
    {
        transform.Translate(speed * Time.deltaTime);
        meteors[select].transform.Rotate(0, 0, 100 * Time.deltaTime);

    }

    public void OnTriggerEnter2D(Collider2D meteoritos)
    {
        if (meteoritos.tag == "Finish")
        {
            Destroy(gameObject);
        } else if (meteoritos.tag == "Bullet")
        {
            //Activa una corutina que se ejecuta paralelamente
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor()
    {
        //Desactivo el grafico
        meteors[select].SetActive(false);

        //Destruye el collider
        Destroy(GetComponent<BoxCollider2D>());

        //Ejecuta la particula
        explosion.Play();

        //Lanzo sonido de explosion
        explosionSound.Play();

        //Instancio meteoritos mas pequeños
        InstanceMeteors();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

    public virtual void InstanceMeteors()
    {
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
    }
    
}
