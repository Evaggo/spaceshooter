﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Vector2 speed = new Vector2(0,0);
    public GameObject bullet;

    private void Start()
    {
        StartCoroutine(EnemyBehaviour());
    }
    private void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }

    IEnumerator EnemyBehaviour()
    {
        while (true)
        {
            //Diagonal arriba
            speed.x = -1;
            speed.y = 1;
            yield return new WaitForSeconds(1.0f);
            //Se para
            speed.x = 0;
            speed.y = 0;
            yield return new WaitForSeconds(1.0f);
            //Dispara
            Instantiate(bullet, transform.position, Quaternion.identity, null);
            //Diagonal abajo
            speed.x = -1;
            speed.y = -1;
            yield return new WaitForSeconds(1.0f);
            //Se para
            speed.x = 0;
            speed.y = 0;
            yield return new WaitForSeconds(1.0f);
            //Dispara
            Instantiate(bullet, transform.position, Quaternion.identity, null);
            
        }
    }
}
