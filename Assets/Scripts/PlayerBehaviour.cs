﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    private Vector2 shipAxis;
    
    public float speed;
    private bool imDead = false;
    private float shootTime = 0f;
    private int lives = 3;

    public Vector2 Limits;
    public Weapon weapon;
    public Propeller prop;
    public GameObject graphics;
    public GameObject propellers;
    public AudioSource explosionSound;
    public ParticleSystem explosion;
    public Collider2D shipCollider;
    public ScoreManager scoreManager;

    void Update () {

        if (imDead == true) {
            return;
        }

        shootTime += Time.deltaTime;

        transform.Translate (shipAxis * speed * Time.deltaTime);

        //Limites de la nave
        if (transform.position.y > Limits.y) { //Limite Arriba

            transform.position = new Vector3(transform.position.x, Limits.y, transform.position.z);
        } else if (transform.position.y < -Limits.y) { //Limite Abajo
            transform.position = new Vector3(transform.position.x, -Limits.y, transform.position.z);
        }

        if (transform.position.x > Limits.x) //Limite derecha
        {
            transform.position = new Vector3(Limits.x, transform.position.y, transform.position.z);
        } else if (transform.position.x < -Limits.x) { //Limite Izquierda
            transform.position = new Vector3(-Limits.x, transform.position.y, transform.position.z);
        }

        //Activar o descativar los propellers
        if (shipAxis.x > 0)
        {
            prop.BlueFire();
        } else if (shipAxis.x < 0) {
            prop.RedFire();
        } else
        {
            prop.Stop();
        }

    }

    private void OnTriggerEnter2D(Collider2D shipCollision)
    {
        if (shipCollision.tag == "Meteor")
        {
            StartCoroutine(DestroyShip());
        }
    }

    public void SetAxis (Vector2 axis) {
            shipAxis = axis;
    }

    public void Shoot() {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0f;
            //Debug.Log ("Pium pium");
            weapon.Shoot();
        }
    }

    IEnumerator DestroyShip()
    {
        imDead = true;
        lives--;
        

        graphics.SetActive(false);
        propellers.SetActive(false);
        shipCollider.enabled = false; 

        explosion.Play();

        explosionSound.Play();

        scoreManager.LoseLife();

        yield return new WaitForSeconds(1.0f);

        if (lives > 0)
        {
            StartCoroutine(RevivirNave());
        } else
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator RevivirNave()
    {
        imDead = false;

        //Blink inmortal nave durante 3 segundos
        for (int i = 0; i<15; i++)
        {
            graphics.SetActive(true);
            propellers.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(false);
            propellers.SetActive(false);
            yield return new WaitForSeconds(0.1f);
        }

        graphics.SetActive(true);
        propellers.SetActive(true);
        shipCollider.enabled = true;

    }


}
