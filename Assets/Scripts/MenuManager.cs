﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        Debug.Log("He pulsado play");
        SceneManager.LoadScene("Game");
    }

    public void PulsaCreditos(){
        Debug.Log("Aqui salen los creditos");
        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit(){
        Debug.Log("Chao pesacao");
        Application.Quit();
    }
}
